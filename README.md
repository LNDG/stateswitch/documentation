The code provides the scripts used to perform the preprocessing and analyses reported in Kosciessa, J.Q., Lindenberger, U. & Garrett, D.D. Thalamocortical excitability modulation guides human perception under uncertainty. Nature Communications 12, 2430 (2021). https://doi.org/10.1038/s41467-021-22511-7

Primary EEG, fMRI, and behavioral data are available from https://osf.io/ug4b8/ (https://doi.org/10.17605/OSF.IO/UG4B8).

## eeg
--------

*preproc*

- preprocess EEG data (see Wiki page in preproc repo)

*ERP*

- calculate ERPs
- assess CPP, peri-response potential

*tfr_periresponse*

- time-frequency analysis on pre-response beta power (contra vs. ipsilateral)

*tfr*

- time-frequency wavelet transform for lower frequency range
- multivariate PLS (requires external toolbox) of load effect (requires gamma data, see below)

*gamma*

- multitaper analysis for gamma range, pre-stim normalization

*ssvep*

- supplementary analysis of 30 Hz SSVEP (based on single-trial MTM with spectral normalization)

*rhythmdetection*

- eBOSC analysis of rhythms during stimulus presentation
- extraction of posterior alpha and frontal theta, linear effects analysis

*cfc*

- cross-frequency coupling between alpha phase and broadband gamma power, using eBOSC's indicated rhythm onsets

*entropy*

- calculate time-resolved sample entropy (1st scale of mMSE toolbox)
- assess linear load effect

*aperiodic*

- calculate linear background spectrum, assess linear load effect


## eye
--------

*preproc*

- read in of ocular edfs to matrices

*pupil*

- preprocessing of pupil recordings, extract trial epochs
- calculate 1st derivative of pupil, assess linear effect & plot

## behavior
--------

This group contains the scripts used for behavioral analyses, as well as multimodal LMMs.

*behavMerge*

- read in raw data and build summary matrix across subjects
- plot main effects, and perform linear mixed effects modeling of RTs and Accs

*HDDM*

- run hierarchical drift-diffusion models, separately for EEG & MRI session
- assess main effects and inter-individual reliability across sessions
- control analysis: target agreement

*multimodal*

- create matrix from individual measures of interest
- export to R
- R: partial repeated measures analyses, bivariate correlations


## fmri_task
--------

This group contains the scripts used for the functional MRI analysis.

*preproc*

- perform fMRI preprocessing (see Methods section)

*preproc_extended*

- DVARS interpolation, CSF + WM regression
- (creates separate output structures used in thalNuclei for plotting)

*spm*

- creates the first level analysis using SPM

*pls*

- performs the second level analysis using the SPM matrices as input
- this contains three main steps: 1) specify the task matrices according to design (2) preparing mean BOLD for PLS using the internal function (3) replace data in PLS-created datamats with 1st level SPM betas
- In addition, the scripts create a mask removing any voxels that have no values in any of the subjects

*thalNuclei*

- visualize thalamic loadings and dynamics